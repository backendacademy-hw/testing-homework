import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class RangeTest extends AnyFlatSpec with Matchers {
  "toString method" should "have form [x; y]" in {
     Range(2, 3).toString shouldBe "[2; 3]"
  }

  "Range()" should "construct empty interval" in {
    Range().toString shouldBe "empty interval"
  }

  "Range(x, y)" should "construct interval [x, y] if x >= y or empty interval" in {
    Range(2, 3).toString shouldBe "[2; 3]"
    Range(3, 2).toString shouldBe "empty interval"
  }

  "isEmpty" should "show empty interval or not" in {
    assert(!Range(1, 2).isEmpty)
    assert(Range().isEmpty)
  }

  "Range contains Range" should "show if the first interval contains the second" in {
    assert(Range(1, 6) contains Range(2, 5))
    assert(Range(1, 6) contains Range(1, 6))
    assert(Range(1, 6) contains Range(1, 2))
    assert(Range(1, 6) contains Range(4, 6))
    assert(Range(1, 6) contains Range())
    assert(Range() contains Range())
  }

  "Range contains point" should "show if the interval contains the point" in {
    assert(Range(1, 6) contains 2)
    assert(!(Range(1, 6) contains 7))
    assert(!(Range() contains 5))
  }

  "Range intersectionWith Range" should "give an intersection of intervals" in {
    Range(3, 5) intersectionWith Range(4, 6) shouldBe Range(4, 5)
    Range(4, 6) intersectionWith Range(3, 5) shouldBe Range(4, 5)
    Range(3, 4) intersectionWith Range(4, 6) shouldBe Range(4, 4)
    Range(3, 5) intersectionWith Range(6, 7) shouldBe Range()
    Range(6, 7) intersectionWith Range(3, 5) shouldBe Range()
    Range(4, 6) intersectionWith Range() shouldBe Range()
  }

  "Range intersects Range" should "show whether ranges intersect" in {
    assert(Range(3, 5) intersects Range(4, 6))
    assert(Range(4, 6) intersects Range(3, 5))
    assert(Range(3, 4) intersects Range(4, 6))
    assert(!(Range(3, 5) intersects Range(6, 7)))
    assert(!(Range(6, 7) intersects Range(3, 5)))
    assert(!(Range(4, 6) intersects Range()))
  }

  it should "return max of nonempty interval" in {
    Range(2, 3).max shouldBe Some(3)
    Range().max shouldBe None
  }

  it should "return min of nonempty interval" in {
    Range(2, 3).min shouldBe Some(2)
    Range().min shouldBe None
  }

  "enumerate" should "return points of nonempty interval" in {
    Range(2, 3).enumerate shouldBe Some(Seq(2, 3))
    Range(4, 4).enumerate shouldBe Some(Seq(4))
    Range().enumerate shouldBe None
  }
}
