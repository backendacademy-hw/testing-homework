import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class AccountTest extends AnyFlatSpec with Matchers {
    it should "return balance of the account" in {
        Account().balance shouldBe 0
    }

    it should "top up the account if top up amount is positive" in {
        Account().topUp(500).balance shouldBe 500
        Account().topUp(100).topUp(200).balance shouldBe 300
        Account().topUp(100).topUp(-200).balance shouldBe 100
    }

    it should "withdraw from the account if the balance allows" in {
        Account().topUp(500).withdraw(200).balance shouldBe 300
        Account().topUp(500).withdraw(600).balance shouldBe 500
        Account().withdraw(100).balance shouldBe 0
    }
}
