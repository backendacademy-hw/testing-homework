import java.time.LocalDateTime

case class Transaction(value: Int, balance: Int, time: LocalDateTime) {
  override def toString = {
    val amount = if (value > 0) f"+$value%-12d" else f"$value%-13d"
    f"$time   $amount  $balance%-10d"
  }
}

class Account private (transactions: List[Transaction] = List()) {
  def balance: Int = if (transactions.nonEmpty) transactions.last.balance else 0

  def topUp(amount: Int): Account = {
    if (amount <= 0) this
    else {
      val transaction = Transaction(amount, this.balance + amount, LocalDateTime.now)
      new Account(transactions :+ transaction)
    }
  }

  def withdraw(amount: Int): Account = {
    if (amount <= 0) this
    else if (amount > this.balance) this
    else {
      val transaction = Transaction(-amount, this.balance - amount, LocalDateTime.now)
      new Account(transactions :+ transaction)
    }
  }

  def mkRecord: String = {
    if (transactions.isEmpty) "No operations for this account"
    else {
      transactions.foldLeft("Time                         Amount         Balance\n")(
        (record, transaction) => record + s"$transaction\n"
      )
    }
  }
}

object Account {
  def apply(): Account = new Account
}
