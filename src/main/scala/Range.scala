class Range private (val borders: Option[(Int, Int)] = None) {
  override def toString: String = borders match {
    case Some((x, y)) => s"[$x; $y]"
    case None => "empty interval"
  }

  override def equals(obj: Any): Boolean = obj match {
    case range: Range => range.borders == this.borders
    case _ => false
  }

  override def hashCode(): Int = borders.hashCode()

  def isEmpty: Boolean = borders match {
    case None => true
    case _ => false
  }

  def contains(that: Range): Boolean = (this.borders, that.borders) match {
    case (None, None) => true
    case (Some(_), None) => true
    case (None, Some(_)) => false
    case (Some((x1, y1)), Some((x2, y2))) =>
      if (x1 <= x2 && y2 <= y1) true else false
  }

  def contains(point: Int): Boolean = this.borders match {
    case Some((x, y)) => x <= point && point <= y
    case None => false
  }

  def intersectionWith(that: Range): Range = (this, that) match {
    case (Range(None), _) | (_, Range(None)) => Range()
    case (range1, range2) =>
      if (range1 contains range2) range2
      else if (range2 contains range1) range1
      else {
        val (Some((x1, y1)), Some((x2, y2))) = (range1.borders, range2.borders)
        if (y1 < x2 || y2 < x1) Range()
        else if (y1 <= y2) Range(x2, y1)
        else Range(x1, y2)
      }
  }

  def intersects(that: Range): Boolean = this intersectionWith that match {
    case Range(Some(_)) => true
    case _ => false
  }

  def max: Option[Int] = this.borders match {
    case Some((_, y)) => Some(y)
    case None => None
  }

  def min: Option[Int] = this.borders match {
    case Some((x, _)) => Some(x)
    case None => None
  }

  def enumerate: Option[Seq[Int]] = this.borders match {
    case Some((x,y)) => Some(x to y)
    case None => None
  }

}

object Range {
  def apply(): Range = new Range()
  def apply(x: Int, y: Int): Range = if (x <= y) new Range(Some((x, y))) else new Range(None)
  def unapply(range: Range): Some[Option[(Int, Int)]] = Some(range.borders)
}
